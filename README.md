# Assemblr

A PHP library to render 'function templates' like components.

## Example

```php
<?php

use Assemblr\StaticAssemblr;

class el extends StaticAssemblr { protected static $instance; }
el::init();

// custom template registration

el::register('html', function ($attrs, callable $children) { ?>
    <!doctype html>
    <html>
    <head>
        <title><?=$attrs['title']?></title>
    </head>
    <body>
        <?php $children(); ?>
    </body>
    </html>
<?php });

// hyperscript like rendering

el::render(
    el::html(
        ['title' => 'Page title'],
        [
            // mixing in custom templates
            el::create(function () { ?>
                <h1>Hello Assemblr!</h1>
            <?php }),
            el::h2([], 'Simple subtitle'),
            el::div(['class' => 'content'], [
                el::p([], 'First paragraph text here'),
                el::p([], 'Second paragraph text here')
            ])
        ]
    )
);
```