<?php

namespace Assemblr\Tests;

require __DIR__.'/../vendor/autoload.php';

use Assemblr\{StaticAssemblr, SingleFileComponent};

class el extends StaticAssemblr {
    protected static $instance;
}
el::init(['Assemblr\Tests\Components' => __DIR__.'/components']);

el::registerSingleFileComponent('html', 'Assemblr\Tests\Components\Html');

// $sfc = new SingleFileComponent(__DIR__.'/components/Html.php');
// $sfc->compile();
// $sfc->load();
// 
// el::register('html', 'Assemblr\Tests\Components\Html\template');

el::html(['title' => 'Hello sfc!'], [
    el::h1([], 'Hello single file component!')
])();