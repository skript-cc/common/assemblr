<?php

namespace Assemblr\Tests;

require __DIR__.'/../vendor/autoload.php';

use Assemblr\{Assemblr, StaticAssemblr};

// static interface
class el extends StaticAssemblr {
    protected static $instance;
}
el::init();

// oop interface
$el = new Assemblr;

// custom template registration

el::register('html', function ($attrs, callable $children) { ?>
    <!doctype html>
    <html>
    <head>
        <title><?=$attrs['title']?></title>
    </head>
    <body>
        <?php $children(); ?>
    </body>
    </html>
<?php });

// hyperscript like rendering

el::render(
    el::html(
        ['title' => 'Page title'],
        [
            // mixing in custom templates
            $el->create(function () { ?>
                <h1>Hello Assemblr!</h1>
            <?php }),
            $el->h2([], 'Simple subtitle'),
            $el->div(classnames(['content', 'mb-5']), [
                $el->p([], 'First paragraph text here'),
                $el->p([], 'Second paragraph text here')
            ])
        ]
    )
);

function classname(string $class, array $attrs=[]) {
    return ['class' => $class] + $attrs;
}

function classnames(array $names, array $attrs=[]) {
    return classname(implode(' ', $names), $attrs);
}