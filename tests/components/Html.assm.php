<?php namespace Assemblr\Tests\Components\Html; ?>
<?php use Assemblr\StaticAssemblr as el; ?>

<?php function style() { ?>
    body {
        background: black;
        color: white;
        font-family: monospace;
    }
<?php } ?>

<?php function script() { ?>(function(window, document, undefined) {
    console.log('Hello from js!');
})(window, document)<?php } ?>

<?php function template($attrs=[], $children) { extract($attrs); ?>
    <!doctype html>
    <html>
    <head>
        <title><?=$attrs['title']?></title>
        <style>
            <?php el::create(__NAMESPACE__.'\style')(); ?>
        </style>
    </head>
    <body>
        <?php $children(); ?>
        <script>
            <?php el::create(__NAMESPACE__.'\script')(); ?>
        </script>
    </body>
    </html>
<?php } ?>