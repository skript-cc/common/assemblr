<?php namespace Assemblr\Tests\Components\Html; ?>
<?php use Assemblr\StaticAssemblr as el; ?>

<style>
    body {
        background: black;
        color: white;
        font-family: monospace;
    }
</style>

<script>
    console.log('Hello from js!');
</script>

<template>
    <!doctype html>
    <html>
    <head>
        <title><?=$attrs['title']?></title>
        <style>
            <?php el::create(__NAMESPACE__.'\style')(); ?>
        </style>
    </head>
    <body>
        <?php $children(); ?>
        <script>
            <?php el::create(__NAMESPACE__.'\script')(); ?>
        </script>
    </body>
    </html>
</template>