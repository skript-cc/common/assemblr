<?php

namespace Assemblr;

class Fn
{
    protected $fn;
    protected $args = [];
    
    public function __construct($fn, ...$args) {
        $this->fn = $fn;
        $this->args = $args;
    }
    
    public function __invoke() {
        $this->beforeInvoke();
        $result = call_user_func_array($this->fn, $this->args);
        $this->afterInvoke();
        return $result;
    }
    
    protected function beforeInvoke() {}
    protected function afterInvoke() {}
}