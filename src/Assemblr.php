<?php

namespace Assemblr;

function findFileByNamespace(string $namespace, array $mapping=[])
{
    $paths = array_filter(
        array_map(
            function ($ns) use ($mapping, $namespace) {
                if (strpos($namespace, $ns) === 0) {
                    $pathSections = explode('/', $mapping[$ns]);
                    $mapNs = explode('\\', $ns);
                    $searchNs = explode('\\', $namespace);
                    return implode('/', array_merge($pathSections, array_diff($searchNs, $mapNs))) . '.php';
                }
                return null;
            },
            $ns = array_keys($mapping)
        ),
        'file_exists'
    );
    return array_pop($paths);
}

class Assemblr
{
    protected $nsFileMap = [];
    protected $register = [];
    
    public function __construct(array $autoloadMap=[]) {
        $this->nsFileMap = $autoloadMap;
    }
    
    public function create(...$args): El {
        return new El(...$args);
    }
    
    public function register($name, callable $el) {
        $this->register[$name] = $el;
    }
    
    public function registerSingleFileComponent(string $name, string $namespace)
    {
        $file = findFileByNamespace($namespace, $this->nsFileMap);
        $sfc = new SingleFileComponent($file);
        $sfc->compile();
        $sfc->load();
        
        $this->register($name, $namespace.'\template');
        $this->register($name.'Style', $namespace.'\style');
        $this->register($name.'Script', $namespace.'\script');
    }
    
    public function render(El $el) {
        return $el();
    }
    
    public function __call(string $name, array $args=[]) {
        return new El($this->register[$name] ?? $name, ...$args);
    }
}