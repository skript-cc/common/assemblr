<?php 

namespace Assemblr;

function children(array $children) {
    foreach ($children as $child) {
        if (is_callable($child)) call_user_func($child);
        else echo $child;
    } 
}

class Children extends El
{
    public function __construct(array $children=[]) {
        $this->fn = __NAMESPACE__.'\children';
        $this->args[] = $children;
    }
    
    public function toArray() {
        return $this->args[0];
    }
}