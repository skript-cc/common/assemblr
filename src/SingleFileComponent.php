<?php

namespace Assemblr;

function compile(string $file)
{
    $handle = fopen($file, "r");
    $out = '';
    $prmap = [
        '/^<style>/' => '<?php function style() { ?>',
        '/^<\/style>/' => '<?php } ?>',
        '/^<script>/' => '<?php function script() { ?>(function(window, document, undefined) {',
        '/^<\/script>/' => '})(window, document)<?php } ?>',
        '/^<template>/' => '<?php function template($attrs=[], $children) { extract($attrs); ?>',
        '/^<\/template>/' => '<?php } ?>'
    ];
    $patterns = array_keys($prmap);
    $replacements = array_values($prmap);
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $out .= preg_replace($patterns, $replacements, $line);
        }
    }
    $assembledFile = str_replace('.php', '.assm.php', $file);
    
    file_put_contents($assembledFile, $out);
    
    return $assembledFile;
}

function load(string $assembledFile) {
    ob_start();
    require_once $assembledFile;
    ob_clean();
}

class SingleFileComponent
{
    protected $file;
    protected $compiledFile;
    
    public function __construct(string $file) {
        $this->file = $file;
    }
    
    public function compile(): self {
        $this->compiledFile = compile($this->file);
        return $this;
    }
    
    public function load(): self {
        load($this->compiledFile);
        return $this;
    }
}