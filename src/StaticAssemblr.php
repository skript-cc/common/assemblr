<?php

namespace Assemblr;

class StaticAssemblr
{
    protected static $instance;
    
    public static function init(array $autoloadMap=[]) {
        self::$instance = new Assemblr($autoloadMap);
    }
    
    public static function create(...$args): El {
        return self::$instance->create(...$args);
    }
    
    public static function register($name, callable $el) {
        return self::$instance->register($name, $el);
    }
    
    public function registerSingleFileComponent(string $name, string $namespace)
    {
        return self::$instance->registerSingleFileComponent($name, $namespace);
    }
    
    public static function render(El $el) {
        return self::$instance->render($el);
    }
    
    public static function __callStatic(string $name, array $args=[]) {
        return self::$instance->$name(...$args);
    }
}