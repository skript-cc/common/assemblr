<?php

namespace Assemblr;

function attrs(array $attributes): string {
    $attrStr = '';
    foreach ($attributes as $key => $value) {
        $quote = strpos($value, '"') !== false ? "'" : '"';
        $attrStr .= ' '.$key.'=';
        $attrStr .= $quote.$value.$quote;
    }
    return $attrStr;
}

function tag(string $name, array $attributes=[], $inner=null) {
    if ($inner) {
        echo '<'.$name.attrs($attributes).'>';
        if (is_callable($inner)) {
            call_user_func($inner);
        } else {
            echo $inner;
        }
        echo '</'.$name.'>';
    } else {
        echo '<'.$name.attrs($attributes).' />';
    }
}

class El extends Fn
{
    protected $isTagFn = false;
    
    public function __construct($type, array $attributes=[], $children=[])
    {
        if ($children && !is_array($children)) {
            $children = [$children];
        }
        
        if (!is_callable($type)) {
            $this->isTagFn = true;
            $args = [__NAMESPACE__.'\tag', $type, $attributes];
            if (count($children)) {
                $args[] = new Children($children);
            }
        } else {
            $args = [$type, $attributes, new Children($children)];
        }
        
        parent::__construct(...$args);
    }
    
    public function __toString() {
        ob_start();
        $this->__invoke();
        return ob_get_clean();
    }
    
    public function getAttributes(): array {
        return $this->args[$this->isTagFn ? 1 : 0];
    }
    
    public function getChildren(): ?Children {
        return $this->args[$this->isTagFn ? 2 : 1];
    }
}